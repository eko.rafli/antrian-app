<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PublicController@index');
// SADMIN
    //Instansi
Route::get('/sadmin/instansi', 'SadminController@instIndex');
Route::get('/sadmin/instansi/{instansi_id}/edit', 'SadminController@instEdit');
Route::put('/sadmin/instansi/{instansi_id}', 'SadminController@instEditAct');
Route::get('/sadmin', function(){
    return redirect('/sadmin/instansi');
});

    //Loket
Route::get('/sadmin/loket', 'SadminController@lktIndex');
Route::get('/sadmin/loket/tambah', 'SadminController@lktTambah');
Route::post('sadmin/loket', 'SadminController@lktTambahAct');
Route::get('/sadmin/loket/{loket_id}/edit', 'SadminController@lktEdit');
Route::put('/sadmin/loket/{loket_id}', 'SadminController@lktEditAct');
Route::delete('/sadmin/loket', 'SadminController@lktHapusAct');

    //Karyawan

Route::get('/sadmin/karyawan', 'SadminController@krywnIndex');
Route::get('/sadmin/karyawan/tambah', 'SadminController@krywnTambah');
Route::post('sadmin/karyawan', 'SadminController@krywnTambahAct');
Route::get('/sadmin/karyawan/{karyawan_id}/edit', 'SadminController@krywnEdit');
Route::put('/sadmin/karyawan/{karyawan_id}', 'SadminController@krywnEditAct');
Route::delete('/sadmin/karyawan', 'SadminController@krywnHapusAct');

    //Agenda

Route::get('/sadmin/agenda', 'SadminController@agendaIndex');
Route::get('/sadmin/agenda/tambah', 'SadminController@agendaTambah');
Route::post('sadmin/agenda', 'SadminController@agendaTambahAct');
Route::get('/sadmin/agenda/{agenda_id}/edit', 'SadminController@agendaEdit');
Route::put('/sadmin/agenda/{agenda_id}', 'SadminController@agendaEditAct');
Route::delete('/sadmin/agenda', 'SadminController@agendaHapusAct');

Route::get('/petugas', 'PetugasController@index');
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
