@extends('layout.mastersadmin')
@section('title')
    Agenda
@endsection
@section('judul')
    Agenda
@endsection
@push('script')
    <script src="{{asset('admin/plugins/datatables/jquery.dataTables.js')}}"></script>
    <script src="{{asset('admin/plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>
    <script>
    $(function () {
        $("#example1").DataTable();
    });
</script>
@endpush
@section('content')

<a class="btn btn-success mb-2" href="/sadmin/agenda/tambah">Tambah</a>
    <table class="table table-bordered">
        <thead>
            <tr>
                <th>#</th>
                <th>Deskripsi Agenda</th>
                <th>File</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            @forelse ($agenda as $key => $value)
            <tr>
                <td>{{$key + 1}}</td>
                <td>{{$value->agenda}}</td>
                <td><img style="width:20rem;" src="{{asset('img/'.$value->file)}}" alt="{{$value->file}}"></td>
                <td>
                    <a class="btn btn-primary mt-2" href="/sadmin/agenda/{{$value->id}}/edit">Edit</a>
                    <form action="/sadmin/agenda/{{$value->id}}" method="POST">
                        @csrf
                        @method('DELETE')
                        <input type="submit" class="btn btn-danger mt-2" value="Hapus">
                    </form>
                </td>
                @empty
                    <td colspan="4">
                        Tidak ada data
                    </td>
            </tr>
            @endforelse
        </tbody>
    </table>

@endsection