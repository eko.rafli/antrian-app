@extends('layout.mastersadmin')
@section('title')
    Tambah Agenda
@endsection
@section('judul')
    Tambah Agenda
@endsection
@push('script')
    <script src="{{asset('admin/plugins/datatables/jquery.dataTables.js')}}"></script>
    <script src="{{asset('admin/plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>
    <script>
    $(function () {
        $("#example1").DataTable();
    });
</script>
@endpush
@section('content')
<form action="/sadmin/agenda" method="POST" enctype="multipart/form-data">
    @csrf
    <div class="form-group">
        <label>Deskripsi</label>
        <input type="text" class="form-control" name="agenda" placeholder="Isikan Deskripsi Agenda">
        @error('agenda')
            <div class="alert alert-danger">
                {{$message}}
            </div>
        @enderror
    </div>
    <div class="form-group">
        <label>File</label>
        <input type="file" class="form-control" name="file">
        <small class="form-text text-muted">Ukuran File Maksimal 2MB</small>
        @error('file')
            <div class="alert alert-danger">
                {{$message}}
            </div>
        @enderror
    </div>
    <button type="submit" class="btn btn-primary">Simpan</button>
    <a href="/sadmin/karyawan" class="btn btn-danger my-1">Batal</a>
    </form>
@endsection