@extends('layout.mastersadmin')
@section('title')
    Karyawan
@endsection
@section('judul')
    Karyawan
@endsection
@push('script')
    <script src="{{asset('admin/plugins/datatables/jquery.dataTables.js')}}"></script>
    <script src="{{asset('admin/plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>
    <script>
    $(function () {
        $("#example1").DataTable();
    });
</script>
@endpush
@section('content')

<a class="btn btn-success mb-2" href="/sadmin/karyawan/tambah">Tambah</a>
    <table class="table table-bordered table-striped">
        <thead class="thead-light">
            <tr>
                <th>Username</th>
                <th>Nama</th>
                <th>Telp</th>
                <th>Level</th>
                <th>Nomor Loket</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            @forelse ($karyawan as $krywn)
            <tr>
                <td>{{$krywn->username}}</td>
                <td>{{$krywn->nama}}</td>
                <td>{{$krywn->telp}}</td>
                <td>@if ($krywn->level === 0)
                    SuperAdmin
                @elseif ($krywn->level === 1)
                    Penjaga Loket
                @elseif ($krywn->level === 2)
                    Penjaga Antrian
                @endif
                </td>
                <td>{{$krywn->loket_id}}</td>
                <td>@if ($krywn->level === 0)
                    <a href="/sadmin/karyawan/{{$krywn->id}}/edit" class="btn btn-primary mx-1" style="float:left;">Edit</a>
                    @else 
                    <a href="/sadmin/karyawan/{{$krywn->id}}/edit" class="btn btn-primary mx-1" style="float:left;">Edit</a>
                    <form action="/sadmin/karyawan/{{$krywn->id}}" method="POST">
                        @csrf
                        @method('DELETE')
                        <input type="submit" class="btn btn-danger" value="Hapus">
                    </form>
                    @endif
                </td>
            </tr>
            @empty
            <td colspan="6">
                Tidak ada data
            </td>              
            @endforelse
        </tbody>
    </table>
@endsection