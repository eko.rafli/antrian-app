@extends('layout.mastersadmin')
@section('title')
    Edit Karyawan
@endsection
@section('judul')
    Edit Karyawan
@endsection
@push('script')
    <script src="{{asset('admin/plugins/datatables/jquery.dataTables.js')}}"></script>
    <script src="{{asset('admin/plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>
    <script>
    $(function () {
        $("#example1").DataTable();
    });
</script>
@endpush
@section('content')
<form action="/sadmin/karyawan/{{$karyawan->id}}" method="POST">
    @csrf
    <div class="form-group">
        <label>Username</label>
        <input type="text" class="form-control text-lowercase" name="username" value="{{$karyawan->username}}" disabled>
        <small class="form-text text-muted">Hanya Huruf Kecil</small>
        @error('username')
            <div class="alert alert-danger">
                {{$message}}
            </div>
        @enderror
    </div>
    <div class="form-group">
        <label>Password</label>
        <div class="input-group" id="show-hide-password">
            <input type="password" class="form-control pwd" name="password" value="" placeholder="Masukkan Password">
            <span class="input-group-btn">
                <button class="btn btn-default reveal" type="button">
                    <i class="fa fa-eye-slash" aria-hidden="true"></i>
                </button>                    
            </span>
        </div>
        <small class="form-text text-muted">Minimal 8 karakter</small>
        @error('password')
            <div class="alert alert-danger">
                {{$message}}
            </div>
        @enderror
    </div>
    <div class="form-group">
        <label>Nama</label>
        <input type="text" class="form-control" name="nama" value="{{$karyawan->nama}}">
        @error('nama')
            <div class="alert alert-danger">
                {{$message}}
            </div>
        @enderror
    </div>
    <div class="form-group">
        <label>Telp</label>
        <input type="number" class="form-control" name="telp" value="{{$karyawan->telp}}">
        @error('telp')
            <div class="alert alert-danger">
                {{$message}}
            </div>
        @enderror
    </div>
    <div class="form-group">
        <label>Level</label>
        <select name="level" class="form-control">
            <option value="1" {{($karyawan->level === 1) ? "selected" : ""}}>Penjaga Loket</option>
            <option value="2" {{($karyawan->level === 2) ? "selected" : ""}}>Penjaga Antrian</option>
        </select>
        @error('level')
            <div class="alert alert-danger">
                {{$message}}
            </div>
        @enderror
    </div>
    <div class="form-group">
        <label>Nomor loket</label>
        <select name="loket_id" class="form-control">
            @foreach ($loket as $lok)
            <option value="{{$lok->id}}" {{($lok->status === 1) ? "disabled" : ""}} {{($karyawan->loket_id === $lok->id) ? "selected" : ""}}>Loket {{$lok->no_loket}}</option>                
            @endforeach
        </select>
        @error('loket_id')
            <div class="alert alert-danger">
                {{$message}}
            </div>
        @enderror
    </div>
    <button type="submit" class="btn btn-primary">Simpan</button>
    <a href="/sadmin/karyawan" class="btn btn-danger my-1">Batal</a>
    </form>
@endsection
@section('footer script')
    @include('scripts.password-script')
@endsection