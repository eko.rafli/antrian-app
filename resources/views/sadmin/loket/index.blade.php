@extends('layout.mastersadmin')
@section('title')
    Setting Loket
@endsection
@section('judul')
    Setting Loket
@endsection
@push('script')
    <script src="{{asset('admin/plugins/datatables/jquery.dataTables.js')}}"></script>
    <script src="{{asset('admin/plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>
    <script>
    $(function () {
        $("#example1").DataTable();
    });
</script>
@endpush
@section('content')

<a class="btn btn-success mb-2" href="/sadmin/loket/tambah">Tambah</a>
    <table class="table table-bordered table-striped">
        <thead class="thead-light">
            <tr>
                <th>Nomor Loket</th>
                <th>Status</th>
                <th>Aksi</th>
            </tr>
        </thead>
        <tbody>
            @forelse ($loket as $lok)
            <tr>
                <td>Loket {{$lok->no_loket}}</td>
                <td>@if ($lok->status === 0)
                    Tidak Aktif
                @elseif ($lok->status === 1)
                    Aktif
                @endif
                </td>
                <td>
                    <a href="/sadmin/loket/{{$lok->id}}/edit" class="btn btn-primary mx-1" style="float:left;">Edit</a>
                    <form action="/sadmin/loket/{{$lok->id}}" method="POST">
                        @csrf
                        @method('DELETE')
                        <input type="submit" class="btn btn-danger" value="Hapus">
                    </form>
                </td>
            </tr>
            @empty
            <td colspan="3">
                Tidak ada data
            </td>              
            @endforelse
        </tbody>
    </table>
@endsection