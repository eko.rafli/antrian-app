@extends('layout.mastersadmin')
@section('title')
    Edit Loket
@endsection
@section('judul')
    Edit Loket
@endsection
@push('script')
    <script src="{{asset('admin/plugins/datatables/jquery.dataTables.js')}}"></script>
    <script src="{{asset('admin/plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>
    <script>
    $(function () {
        $("#example1").DataTable();
    });
</script>
@endpush
@section('content')
<form action="/sadmin/loket/{{$loket->id}}" method="POST">
    @csrf
    @method('PUT')
    <div class="form-group">
        <label>Nomor Loket</label>
        <input type="number" class="form-control" name="no_loket" value="{{$loket->no_loket}}" disabled>
        @error('no_loket')
            <div class="alert alert-danger">
                {{$message}}
            </div>
        @enderror
    </div>
    <div class="form-group">
        <label>Status</label>
        <select name="status" class="form-control">
            <option value="0" {{($loket->status === 0) ? "selected" : ""}}>Tidak Aktif</option>
            <option value="1" {{($loket->status === 1) ? "selected" : ""}}>Aktif</option>
        </select>
        @error('status')
            <div class="alert alert-danger">
                {{$message}}
            </div>
        @enderror

    <button type="submit" class="btn btn-primary">Simpan</button>
    <a href="/sadmin/loket" class="btn btn-danger my-1">Batal</a>
    </form>
@endsection