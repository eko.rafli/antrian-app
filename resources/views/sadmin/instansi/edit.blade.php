@extends('layout.mastersadmin')
@section('title')
    Edit Profil Instansi
@endsection
@section('judul')
    Edit Profil Instansi
@endsection
@section('content')
    <form action="/sadmin/instansi/{{$instansi->id}}" method="POST" enctype="multipart/form-data">
    @csrf
    @method('PUT')
    <div class="form-group">
        <label>Nama Instansi</label>
        <input type="text" class="form-control" name="nama" value="{{$instansi->nama}}">
        @error('nama')
            <div class="alert alert-danger">
                {{$message}}
            </div>
        @enderror
    </div>
    <div class="form-group">
        <label>Alamat</label>
        <textarea name="alamat" class="form-control">{{$instansi->alamat}}</textarea>
        @error('alamat')
            <div class="alert alert-danger">
                {{$message}}
            </div>
        @enderror
    </div>
    <div class="form-group">
        <label>No. Telepon</label>
        <input type="number" class="form-control" name="telp" value="{{$instansi->telp}}">
        @error('telp')
            <div class="alert alert-danger">
                {{$message}}
            </div>
        @enderror
    </div>
    <div class="form-group">
        <label>Logo</label>
        <input type="file" class="form-control" name="logo">
        @error('logo')
            <div class="alert alert-danger">
                {{$message}}
            </div>
        @enderror
    </div>
    <button type="submit" class="btn btn-primary">Simpan</button>
    <a href="/sadmin" class="btn btn-danger my-1">Batal</a>
    </form>
@endsection