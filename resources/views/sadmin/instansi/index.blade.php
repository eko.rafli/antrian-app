@extends('layout.mastersadmin')
@section('title')
    Profil Instansi
@endsection
@section('judul')
    Profil Instansi
@endsection
@push('script')
    <script src="{{asset('admin/plugins/datatables/jquery.dataTables.js')}}"></script>
    <script src="{{asset('admin/plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>
    <script>
    $(function () {
        $("#example1").DataTable();
    });
</script>
@endpush
@section('content')
    <table class="table table-bordered">
        <thead>
            <tr>
                <th>Nama Instansi</th>
                <th>Alamat</th>
                <th>No. Telepon</th>
                <th>Logo</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>{{$instansi->nama}}</td>
                <td>{{$instansi->alamat}}</td>
                <td>{{$instansi->telp}}</td>
                <td><img src="{{asset('img/'.$instansi->logo)}}" alt="{{$instansi->logo}}"></td>
            </tr>
        </tbody>
    </table>
    <a class="btn btn-danger mt-2" href="/sadmin/instansi/{{$instansi->id}}/edit">Edit</a>
@endsection