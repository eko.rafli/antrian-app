<script>
$(".reveal").on('mousedown',function() {
    var $pwd = $(".pwd");
    if ($pwd.attr('type') === 'password') {
        $pwd.attr('type', 'text');
    } else {
        $pwd.attr('type', 'password');
    }
});

$(".reveal").on('mouseup',function() {
    var $pwd = $(".pwd");
    if ($pwd.attr('type') === 'text') {
        $pwd.attr('type', 'password');
    } else {
        $pwd.attr('type', 'password');
    }
});
</script>