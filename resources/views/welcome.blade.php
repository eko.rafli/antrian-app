<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Antrian</title>
        <!-- Title Icon -->
        <link rel="icon" href="{{asset('img/logo.png')}}">

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css2?family=Lato:wght@300;700;900&display=swap" rel="stylesheet">

        <!-- Styles -->
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

        <style>
            body {
                font-family: 'Nunito', sans-serif;
            }

            .logotxt{
                font-family: 'Lato', sans-serif;
                font-size: 3em;
                text-decoration: none;
            }
        </style>
        <style>
        </style>
    </head>
    <body>
            <div class="sticky-top">
                <nav class="navbar navbar-light" style="background-color: #e3f2fd;">
                    <div class="container-fluid d-flex justify-content-between">
                        <img src="{{asset('img/logo.png')}}" height="50rem" alt="">
                        <span class="navbar-text">{{$instansi->alamat}}</span>
                    </div>
                </nav>
            </div>
            <div class="row mx-0 mt-3">
                <div class="card" style="margin-right:2%;margin-left:1%;width:40%;">
                    <div id="galeri" class="carousel slide" data-ride="carousel" style="">
                        <div class="carousel-inner">
                            @foreach ($agenda as $key => $slider)
                            <div class="carousel-item {{$key == 0 ? 'active' : ''}}" data-interval="2000" style="width:40rem;height:37rem;background-position: center center;background-repeat: no-repeat;">
                                <img class="d-block img-fluid" style="min-weight:100%;min-height:100%;object-fit:cover;object-position:center;" src="{{asset('img/'.$slider->file)}}" alt="">
                            </div>
                            @endforeach
                        </div>
                    </div>
                </div>
                <!--<div class="card" style="margin-right:2%; width:55%;">
                    <div class="card-body">
                        @yield('loket')
                    </div>
                </div>-->
            </div>
            <footer class="bg-light text-center text-lg-start fixed-bottom">
                <!-- Copyright -->
                <div class="d-flex justify-content-between p-3 " style="background-color: rgba(0, 0, 0, 0.2);">
                  <a class="text-success text-decoration-none" href="/ambilnomor">Ambil Nomor Antrian</a>
                  <a class="text-danger text-decoration-none" href="/login">Login</a>
                </div>
                <!-- Copyright -->
              </footer>
    
            
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>
    </body>
</html>
