<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Loket;
use App\Agenda;
use App\Instansi;

class PublicController extends Controller
{
    public function index(){
        $loket = Loket::all();
        $agenda = Agenda::all();
        $instansi = Instansi::firstOrFail();
        return view('welcome', compact('loket', 'agenda', 'instansi'));
    }
}
