<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Instansi;
use App\Loket;
use App\Karyawan;
use App\Agenda;

class SadminController extends Controller
{
// INSTANSI
    public function instIndex(){
        $instansi = Instansi::firstOrFail();
        return view('sadmin.instansi.index', compact('instansi'));
    }

    public function instEdit($id){
        $instansi = Instansi::find($id);
        return view('sadmin.instansi.edit', compact('instansi'));
    }

    public function instEditAct(Request $request){
        $request->validate([
            'nama' => 'required',
            'alamat' => 'required',
            'telp' => 'required',
            'logo' => 'required|image|mimes:jpeg,jpg,png,gif,svg|max:2048'
        ]);
        $imgName = 'logo.' .$request->logo->extension();
        $request->logo->move(public_path('img'), $imgName);
        $instansi = Instansi::firstOrFail();
        $instansi->nama = $request->nama;
        $instansi->alamat = $request->alamat;
        $instansi->telp = $request->telp;
        $instansi->logo = $imgName;
        $instansi->update();
        return redirect('/sadmin/instansi');
    }

// LOKET
    public function lktIndex(){
        $instansi = Instansi::firstOrFail();
        $loket = Loket::all();
        return view('sadmin.loket.index', compact('loket','instansi'));
    }

    public function lktTambah(){
        $instansi = Instansi::firstOrFail();
        return view('sadmin.loket.tambah', compact('instansi'));
    }

    public function lktTambahAct(Request $request){
        $this->validate($request,[
            'no_loket' => 'required|unique:loket',
            'status' => 'required'
        ]);
        Loket::create([
            'no_loket' => $request->no_loket,
            'status'=> $request->status
        ]);
        return redirect('/sadmin/loket');
    }

    public function lktEdit($id){
        $instansi = Instansi::firstOrFail();
        $loket = Loket::find($id);
        return view('sadmin.loket.edit', compact('loket','instansi'));
    }

    public function lktEditAct(Request $request, $id){
        $this->validate($request,[
            'status' => 'required'
        ]);
        $loket = Loket::find($id);
        $loket->status = $request->status;
        $loket->update();
        return redirect('/sadmin/loket');
    }

    public function lktHapusAct($id){
        $loket = Loket::find($id);
        $loket -> delete();
        return redirect('/sadmin/loket');
    }

// KARYAWAN
    public function krywnIndex(){
        $instansi = Instansi::firstOrFail();
        $karyawan = Karyawan::all();
        $loket = Loket::all();
        return view('sadmin.karyawan.index', compact('karyawan','instansi', 'loket'));
    }

    public function krywnTambah(){
        $instansi = Instansi::firstOrFail();
        $karyawan = Karyawan::all();
        $loket = Loket::all();
        return view('sadmin.karyawan.tambah', compact('instansi', 'karyawan', 'loket'));
    }

    public function krywnTambahAct(Request $request){
        $this->validate($request,[
            'username' => 'required|unique:karyawan',
            'password' => 'required|min:8',
            'nama' => 'required',
            'telp' => 'required',
            'level' => 'required'
        ]);
        $passw = Hash::make($request->password);
        Karyawan::create([
            'username' => strtolower($request->username),
            'password' => $passw,
            'nama' => $request->nama,
            'telp' => $request->telp,
            'level' => $request->level,
            'loket_id' => $request->loket_id
        ]);
        return redirect('/sadmin/karyawan');
    }

    public function krywnEdit($id){
        $instansi = Instansi::firstOrFail();
        $karyawan = Karyawan::find($id);
        $loket = Loket::all();
        return view('sadmin.karyawan.edit', compact('karyawan','instansi', 'loket'));
    }

    public function krywnEditAct(Request $request, $id){
        $this->validate($request,[
            'password' => 'requiredmin:8',
            'nama' => 'required',
            'telp' => 'required',
            'level' => 'required',
            'loket_id' => 'required'
        ]);
        $passw = Make::hash($request->password);
        $karyawan = Karyawan::find($id);
        $karyawan->nama = $request->nama;
        $karyawan->password = $passw;
        $karyawan->telp = $request->telp;
        $karyawan->level = $request->level;
        $karyawan->loket_id = $request->loket_id;
        $karyawan->update();
        return redirect('/sadmin/karyawan');
    }

    public function krywnHapusAct($id){
        $loket = Karyawan::find($id);
        $loket -> delete();
        return redirect('/sadmin/karyawan');
    }

// KARYAWAN
    public function agendaIndex(){
        $instansi = Instansi::firstOrFail();
        $agenda = Agenda::all();
        return view('sadmin.agenda.index', compact('agenda','instansi'));
    }

    public function agendaTambah(){
        $instansi = Instansi::firstOrFail();
        $agenda = Agenda::all();
        return view('sadmin.agenda.tambah', compact('instansi', 'agenda'));
    }

    public function agendaTambahAct(Request $request){
        $this->validate($request,[
            'agenda' => 'required',
            'file' => 'required|image|mimes:jpeg,jpg,png,gif,svg|max:2048'
        ]);
        $imgName = 'agenda_'. time() . '.' .$request->file->extension();
        $request->file->move(public_path('img'), $imgName);
        Agenda::create([
            'agenda' => $request->agenda,
            'file' => $imgName
        ]);
        return redirect('/sadmin/agenda');
    }

    public function agendaEdit($id){
        $instansi = Instansi::firstOrFail();
        $agenda = Agenda::find($id);
        return view('sadmin.agenda.edit', compact('agenda','instansi'));
    }

    public function agendaEditAct(Request $request, $id){
        $this->validate($request,[
            'agenda' => 'required',
            'file' => 'required'
        ]);
        $agenda = Agenda::find($id);
        $agenda->agenda = $request->agenda;
        $agenda->file = $request->nama;
        $agenda->update();
        return redirect('/sadmin/agenda');
    }

    public function agendaHapusAct($id){
        $loket = Karyawan::find($id);
        $loket -> delete();
        return redirect('/sadmin/agenda');
    }    
}