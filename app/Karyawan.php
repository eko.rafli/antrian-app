<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Karyawan extends Model
{
    protected $table = 'karyawan';
    protected $fillable = ['username', 'password', 'nama', 'telp', 'level', 'loket_id'];
}
