<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Loket extends Model
{
    protected $table = "loket";
    protected $fillable = ["no_loket","status"];
}
